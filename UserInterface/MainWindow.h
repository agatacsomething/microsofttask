/**
 * File: MainWindow
 * Description: This is the main window that runs the application. It is made of up a simple QT widget with
 * 		menu of buttons to navigate app, a text box to print data, a graph area to plot data and a table
 * 		area to print information from the tests.
 * Menu Buttons:
 * 		loadButton - connects to loadfile()
 * 			loadfile calls the class Preprocessdata to store the loaded text file. By default this data is
 * 			split into train and test sets in a 80/20 ratio
 * 			associated variables:
 * 				Preprocessdata fd
 * 				ParsedData *pd_train;
 * 				ParsedData *pd_test;
 * 				inputrelevButton
 *
 * 		inputrelevButton - connects to plot_inputrelev()
 * 			plots the count of relevant documents per query for the original input dataset
 *
 * 		shuffleDataButton - connects to shuffle_data_btn()
 * 			shuffle_data_btn disposes of the current data stored in train and test sets and replaces them
 * 			with a new random shuffling of data
 * 			ssociated variables:
 * 				Preprocessdata fd
 * 				ParsedData *pd_train;
 * 				ParsedData *pd_test;
 *
 * 		algo_menu - a menu of buttons which allow the user to choose which algorithms to apply to the data
 * 			associated functions:
 * 				apply_lr() - applies logistic regression by calling the class LogReg then calls class Evalresults to process data
 * 				associated variables:
 * 						Evalresults *lr_eval; - stores the ranked results per query search
 *				apply_lr_rank(); - applies linear svmrank by calling the class LinRank then calls class Evalresults to process data
 *				associated variables:
 *						Evalresults *linrank_eval; - stores the ranked results per query search
 *				apply_libsvmrank(); - applies non-linear svmrank by calling the class LinRank then calls class Evalresults to process data
 *				associated variables:
 *						Evalresults *svmrank_eval; - stores the ranked results per query search
 *				apply_all();
 *						applies all 3 algorithms successively
 *
 *		weight_menu- a menu of buttons which allow the user to plot the learned weights from log reg and lin svmrank
 *			associated functions:
 *					void plot_weights_lr();
 *					void plot_weights_lrrank();
 *
 *		map_menu- a menu of buttons which allow the user to plot the mean average precision for the tested data
 *			associated functions:
 *					void plot_map(std::vector<double> map_vals);
 *					void plot_lrmap();
 *					void plot_lrrankmap();
 *					void plot_svmrankmap();
 */


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <boost/chrono.hpp>

#include <QtGui>
#include <QDebug>
#include <QMainWindow>
#include <QApplication>
#include <QTextEdit>
#include <QPushButton>
#include <QMenu>
#include <QSignalMapper>
#include <QTableWidget>
#include <QStackedWidget>
#include "qcustomplot.h"

#include "preprocessdata.h"
#include "logreg.h"
#include "linrank.h"
#include "libsvmrank.h"
#include "evalresults.h"

 class MainWindow : public QWidget
 {
     Q_OBJECT

public:
     MainWindow(QWidget *parent = 0);

	QCustomPlot * customPlot;
	PreprocessData * fd; //full dataset
	QCPBars *relevPd_plot;
	QCPBars *lr_bar;
	QCPBars *lrrank_bar;
	QCPBars *svm_bar;


private slots:
	void loadFile();
	void shuffle_data_btn();
	void apply_lr();
	void apply_lr_rank();
	void apply_libsvmrank();
	void apply_all();


	//analysis functions
	void plot_inputrelev();
	void plot_weights_lr();
	void plot_weights_lrrank();
	//void plot_results();
	void plot_maptable(std::vector<double> map_vals, double total_map, int which_table);
	void plot_map(std::vector<double> map_vals);
	void plot_lrmap();
	void plot_lrrankmap();
	void plot_svmrankmap();
	void plot_maptable();
	void lrVSlrrank();
	void lrVSsvmrank();
	void lrrankVSsvmrank();


private:
	QSplitter *mainSplitter;
	QGridLayout *mainLayout;
	QMenu *map_menu;
	QMenu *algo_menu;
	QMenu *sigtest_menu;
	QMenu *weight_menu;
	QVBoxLayout *buttonLayout1;
	QPushButton *loadButton;
	QPushButton *shuffleDataButton;
	QPushButton *quitButton;
	QPushButton *apply_algos;

	QTableWidget *mapTable;
	QStackedWidget *stackedWidget;
	QHBoxLayout *toplayout;
	QHBoxLayout *bottomlayout;

	//clicked buttons
	bool shuffled;
	double lr_time, lrrank_time, svm_time;

	QTextEdit *textEdit;

	//buttons for analysis
	QPushButton *inputrelevButton;
	QPushButton *svplotButton;
	QPushButton *plotresultsButton;
	QPushButton *sigtestButton;

	//data
	ParsedData *pd_train;
	ParsedData *pd_test;

	//data storage for analysis
	Evalresults *lr_eval;
	Evalresults *linrank_eval;
	Evalresults *svmrank_eval;


	void updatetext(QString str);
	void helloWindow();
	void createMenus();
	void createButtons();
	void printSummary();
	void plotRelevPD();
	void shuffle_data();
	void plot_weights(double *w);
	void plot_map_all();
	void apply_zerotest(int which_test);
	void printResults(QString test_id);

};

#endif
