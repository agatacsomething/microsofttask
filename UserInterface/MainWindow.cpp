#include "MainWindow.h"

#include <iostream>
#include <numeric>

#include "compmethods.h"

MainWindow::MainWindow(QWidget *parent)
: QWidget(parent), fd(NULL), pd_train(NULL), pd_test(NULL), relevPd_plot(NULL), lr_eval(NULL), linrank_eval(NULL), svmrank_eval(NULL)
{
	//setting up the main layout for the widget: buttons, text area, plotting area
	textEdit = new QTextEdit;
	textEdit->setReadOnly(true);
	textEdit->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
	helloWindow();
	customPlot = new QCustomPlot(this);
	customPlot->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

	mapTable = new QTableWidget;
	mapTable->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	mapTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
	mapTable->setMaximumWidth(300);

	createButtons();
	createMenus();

	toplayout= new QHBoxLayout;
	toplayout->addLayout(buttonLayout1);
	toplayout->addWidget(textEdit);

	bottomlayout= new QHBoxLayout;
	bottomlayout->addWidget(mapTable);
	bottomlayout->addWidget(customPlot);

	mainLayout = new QGridLayout;
	mainLayout->addLayout(toplayout, 0, 0);
	mainLayout->addLayout(bottomlayout, 1, 0);
	mainLayout->setRowStretch( 0, 0 ) ;
	mainLayout->setRowStretch( 1, 4 ) ;

    setLayout(mainLayout);
}

void MainWindow::helloWindow(){
	textEdit->append("This a machine learning mini-gui. \nTo use this app you must first enter a text file in the format below");
	textEdit->append("\n");
	textEdit->append("1 qid:WT04-170 1:0.033761 2:0.031250 3:0.076923 4:0.000000 #docid = G06-90-0596202 ");
	textEdit->append("      1. '1' represents the query reference label");
	textEdit->append("      2. 'WT04-170' represents the query ID");
	textEdit->append("      3. '1:0.033761 ... x:0.038514' represent the precomputed features for the documents");
	textEdit->append("      4. 'G06-90-0596202' represents the document ID");
	textEdit->append("\n");
	textEdit->append("When the data is loaded, you can then run training algorithms on the data and compare the algorithms against "
					 "each other using the aggressive sign test or less aggressive z-test.");
	textEdit->append("\n");
	textEdit->append("The training algorithms are as follows: ");
	textEdit->append("      1. Logistic regression");
	textEdit->append("      2. Linear SVMRANK");
	textEdit->append("      3. Non-Linear (RBF) SVMRANK (this can take up to 5 minutes, so be patient)");


}

void MainWindow::createButtons(){
	loadButton = new QPushButton(tr("&Load"));
	shuffleDataButton = new QPushButton(tr("&Shuffle Data"));
	inputrelevButton = new QPushButton(tr("&Plot input relevances"));
	apply_algos= new QPushButton(tr("&Apply Learning Algo"));
	svplotButton = new QPushButton(tr("&Plot learned weights"));
	plotresultsButton = new QPushButton(tr("&Plot MAP"));
	sigtestButton = new QPushButton(tr("&Compare Algos"));
	quitButton = new QPushButton(tr("&Quit"));

	connect(loadButton, SIGNAL(clicked()), this, SLOT(loadFile()));
	connect(shuffleDataButton, SIGNAL(clicked()), this, SLOT(shuffle_data_btn()));
	connect(inputrelevButton, SIGNAL(clicked()), this, SLOT(plot_inputrelev()));
	//connect(svplotButton, SIGNAL(clicked()), this, SLOT(plot_sv()));
	connect(quitButton, SIGNAL(clicked()), qApp, SLOT(quit()));

	buttonLayout1 = new QVBoxLayout;
	buttonLayout1->addWidget(loadButton, Qt::AlignTop);
	buttonLayout1->addWidget(shuffleDataButton);
	buttonLayout1->addWidget(inputrelevButton);
	buttonLayout1->addWidget(apply_algos);
	buttonLayout1->addWidget(svplotButton);
	buttonLayout1->addWidget(plotresultsButton);
	buttonLayout1->addWidget(sigtestButton);
	buttonLayout1->addWidget(quitButton);
	buttonLayout1->addStretch();
}

void MainWindow::createMenus(){
	//setting up the 4 menus
	map_menu = new QMenu();
	QAction *lrmap;
	lrmap = new QAction(tr("&Lr MAP"), this);
	map_menu->addAction(lrmap);
    connect(lrmap, SIGNAL(triggered()), this, SLOT(plot_lrmap()));
	QAction *lrrankmap;
	lrrankmap = new QAction(tr("&Linear svmrank MAP"), this);
	map_menu->addAction(lrrankmap);
    connect(lrrankmap, SIGNAL(triggered()), this, SLOT(plot_lrrankmap()));
	QAction *svmrankmap;
	svmrankmap = new QAction(tr("&SVMrank MAP"), this);
	map_menu->addAction(svmrankmap);
    connect(svmrankmap, SIGNAL(triggered()), this, SLOT(plot_svmrankmap()));
	QAction *allmaptable;
	allmaptable = new QAction(tr("&Table of all MAPs"), this);
	map_menu->addAction(allmaptable);
    connect(allmaptable, SIGNAL(triggered()), this, SLOT(plot_maptable()));
	plotresultsButton->setMenu(map_menu);

	algo_menu = new QMenu();
	QAction *lralgo;
	lralgo = new QAction(tr("&Logistic Regression"), this);
	algo_menu->addAction(lralgo);
    connect(lralgo, SIGNAL(triggered()), this, SLOT(apply_lr()));
    QAction *lrrankalgo;
    lrrankalgo = new QAction(tr("&Linear SVMRank"), this);
    algo_menu->addAction(lrrankalgo);
    connect(lrrankalgo, SIGNAL(triggered()), this, SLOT(apply_lr_rank()));
    QAction *svmrankalgo;
    svmrankalgo = new QAction(tr("&SVM Rank"), this);
    algo_menu->addAction(svmrankalgo);
    connect(svmrankalgo, SIGNAL(triggered()), this, SLOT(apply_libsvmrank()));
    QAction *allalgo;
    allalgo = new QAction(tr("&All algorithms"), this);
    algo_menu->addAction(allalgo);
    connect(allalgo, SIGNAL(triggered()), this, SLOT(apply_all()));
    apply_algos->setMenu(algo_menu);

    sigtest_menu = new QMenu();
    QAction *lrVSlrrankButton;
    lrVSlrrankButton = new QAction(tr("&LR vs LinSVM"), this);
    sigtest_menu->addAction(lrVSlrrankButton);
    connect(lrVSlrrankButton, SIGNAL(triggered()), this, SLOT(lrVSlrrank()));
    QAction *lrVSsvmrankButton;
    lrVSsvmrankButton = new QAction(tr("&LR vs non-LinSVM"), this);
    sigtest_menu->addAction(lrVSsvmrankButton);
    connect(lrVSsvmrankButton, SIGNAL(triggered()), this, SLOT(lrVSsvmrank()));
    QAction *lrrankVSsvmrankButton;
    lrrankVSsvmrankButton = new QAction(tr("&LinSVM vs non-LinSVM"), this);
    sigtest_menu->addAction(lrrankVSsvmrankButton);
    connect(lrrankVSsvmrankButton, SIGNAL(triggered()), this, SLOT(lrrankVSsvmrank()));
    sigtestButton->setMenu(sigtest_menu);

    weight_menu = new QMenu();
    QAction *weights_lr;
    weights_lr = new QAction(tr("&Log Reg weights"), this);
    weight_menu->addAction(weights_lr);
    connect(weights_lr, SIGNAL(triggered()), this, SLOT(plot_weights_lr()));
    QAction *weights_lrrank;
    weights_lrrank = new QAction(tr("&LinSVM weights"), this);
    weight_menu->addAction(weights_lrrank);
    connect(weights_lrrank, SIGNAL(triggered()), this, SLOT(plot_weights_lrrank()));
    svplotButton->setMenu(weight_menu);

}

void MainWindow::loadFile(){
	textEdit->clear();

	//check to see if there has been data loaded before, if so wipe previous instance
	if (fd){
		delete fd;
	}
	fd = new PreprocessData();

	//prompt user to enter in text file
	 QString fileName = QFileDialog::getOpenFileName(this,
	         tr("Open Data File"), "",
	         tr("Data File (*.txt);;All Files (*)"));
	 if (fileName.isEmpty())
	          return;
	 else{
		 fd->loadDataFile(fileName.toStdString());
		 shuffled = false;
		 shuffle_data();
	 }

	 //once data is loaded print statistics and show visual
	 printSummary();
	 plotRelevPD();
	 std::vector<double> rel_per_query(fd->pd->data_stats.rel_per_query.begin(), fd->pd->data_stats.rel_per_query.end());

	 double sum = std::accumulate(fd->pd->data_stats.rel_per_query.begin(), fd->pd->data_stats.rel_per_query.end(), 0.0);
	 double avg_doc= sum/fd->pd->data_stats.uniq_queries.size();
	 plot_maptable(rel_per_query, avg_doc, 1);
}

void MainWindow::shuffle_data_btn(){
	if (!fd){
		QMessageBox::critical(this, "Error", "Please load an appropriate dataset !");
	}
	else{
		shuffled = false;
		shuffle_data();
	}
}

void MainWindow::shuffle_data(){
	if(!shuffled){
		if(pd_train){
			delete pd_train;
			delete pd_test;
		}
		pd_train = new ParsedData;
		pd_test  = new ParsedData;
		PreprocessData::SplitData(fd->pd->parsed_data, pd_train, pd_test, 2);
	}
	shuffled = true;
}

void MainWindow::plotRelevPD(){

	customPlot->clearGraphs();
	if(relevPd_plot){
		customPlot->removePlottable(relevPd_plot);
	}

	if(lr_bar && lrrank_bar && svm_bar){
		customPlot->removePlottable(lr_bar);
		customPlot->removePlottable(lrrank_bar);
		customPlot->removePlottable(svm_bar);
	}

	relevPd_plot = new QCPBars(customPlot->xAxis, customPlot->yAxis);

	customPlot->show();
	customPlot->addPlottable(relevPd_plot);


	QPen pen;
	pen.setWidthF(1.2);
	relevPd_plot->setName("Fossil fuels");
	relevPd_plot->setPen(pen);
	relevPd_plot->setBrush(QColor(255, 131, 0, 50));

	// prepare axes:
	QVector<double> ticks;
	QVector<QString> labels;

	int i=0;
	for (std::vector<std::string>::iterator it = fd->pd->data_stats.uniq_queries.begin() ; it != fd->pd->data_stats.uniq_queries.end(); ++it){
		QString qstr = QString::fromStdString(*it);
		labels.push_back(qstr);
		ticks.push_back(i+1);
		i++;
	}

	customPlot->xAxis->setAutoTicks(false);
	customPlot->xAxis->setAutoTickLabels(false);
	customPlot->xAxis->setTickVector(ticks);
	customPlot->xAxis->setTickVectorLabels(labels);
	customPlot->xAxis->setTickLabelRotation(-60);
	customPlot->yAxis->setLabel("Number of Relevant \nDocuments per Query");

	//Set data for Plot
	QVector<double> valueData;

	for (std::vector<int>::iterator it = fd->pd->data_stats.rel_per_query.begin() ; it != fd->pd->data_stats.rel_per_query.end(); ++it){
		valueData.push_back(double(*it));
	}

	relevPd_plot->setData(ticks, valueData);

	customPlot->legend->setVisible(false);
	//Plot it
	customPlot->rescaleAxes();
	customPlot->replot();

}

void MainWindow::apply_lr(){
	QString q = "Applying logistic regression.";
	updatetext(q);

	if (!fd){
		QMessageBox::critical(this, "Error", "Please load an appropriate dataset !");
	}
	else{
		if(lr_eval){
			delete lr_eval;
		}

		//run and time train data
		LogReg *lr = new LogReg();
	    boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();
	    lr->logreg_train(*pd_train);
	    boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
	    lr_time = sec.count();

	    //save model
		const char *model_file_name = "saved_model.model";
		linear_model *train_model;
		train_model =load_model(model_file_name);

		//test algo with learned model on test set
		lr->logreg_test(*pd_test, train_model);

		//evaluate test results
		lr_eval = new Evalresults(lr->accuracy);
		lr_eval->calc_map(lr->raw_ranks2, *pd_test, 2);

		//show results
		QString test_id = "Log Regression";
		printResults(test_id);
		plot_maptable(lr_eval->out_res->map_vals_raw, lr_eval->out_res->total_map, 2);
		plot_map(lr_eval->out_res->map_vals_raw);
	}
}

void MainWindow::updatetext(QString str){
	textEdit->clear();
	textEdit->append(str);
}

void MainWindow::apply_lr_rank(){
	qDebug() << "applying_lr RANK...";
	QString q = "Applying LinearSVM rank. Please be patient, this could take up to 5 minutes based on the size of the training set.";
	updatetext(q);

	if (!fd){
		QMessageBox::critical(this, "Error", "Please load an appropriate dataset !");
	}
	else{
		if(linrank_eval){
			delete(linrank_eval);
		}

		//run and time train data
		LinRank *linrank = new LinRank();
	    boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();
	    linrank->linrank_train(*pd_train);
	    boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
	    lrrank_time = sec.count();

	    //save model
	    const char *model_file_name = "saved_lrrank_model.model";
		linear_model *train_model;
		train_model =load_model(model_file_name);

		//test algo with learned model on test set
		linrank->linrank_test(*pd_test, train_model);
		linrank_eval = new Evalresults(linrank->pairwise_acc, linrank->ndcg_acc);
		linrank_eval->calc_map(linrank->raw_ranks2, *pd_test, 2);

		//show results
		QString test_id = "LR Rank";
		printResults(test_id);
		plot_maptable(linrank_eval->out_res->map_vals_raw, linrank_eval->out_res->total_map, 3);
		plot_map(linrank_eval->out_res->map_vals_raw);
	}


}

void MainWindow::apply_libsvmrank(){
	qDebug() << "apply_libsvmrank RANK...";
	QString q = "Applying SVM rank. Please be patient, this could take up to 5 minutes based on the size of the training set.";
	updatetext(q);

	if (!fd){
		QMessageBox::critical(this, "Error", "Please load an appropriate dataset !");
	}
	else{
		if(svmrank_eval){
			delete(svmrank_eval);
		}

		//run and time train data
		Libsvmrank *svmrank = new Libsvmrank();
		boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();
		svmrank->libsvmrank_train(*pd_train);
		boost::chrono::duration<double> sec = boost::chrono::system_clock::now() - start;
		svm_time = sec.count();

		//save learned model
		const char *model_file_name = "saved_libsvmrank_model.model";
		svm_model *train_model;
		train_model =svm_load_model(model_file_name);

		//test algo with learned model on test set
		svmrank->libsvmrank_test(*pd_test, train_model);
		svmrank_eval = new Evalresults(svmrank->pairwise_acc, svmrank->ndcg_acc);
		svmrank_eval->calc_map(svmrank->raw_ranks2, *pd_test, 2);

		//show results
		QString test_id = "SVM Rank";
		printResults(test_id);
		plot_maptable(svmrank_eval->out_res->map_vals_raw, svmrank_eval->out_res->total_map, 4);
		plot_map(svmrank_eval->out_res->map_vals_raw);
	}
}

void MainWindow::apply_all(){
	textEdit->clear();
	textEdit->append("Applying all algorithms. Please be patient, this could take up to 5 minutes based on the size of the training set. ");
	if (!fd){
		QMessageBox::critical(this, "Error", "Please load an appropriate dataset !");
	}
	else{
		apply_lr();
		apply_lr_rank();
		apply_libsvmrank();
	}
	QString test_id = "all";
	printResults(test_id);
	plot_map_all();
	plot_maptable();
}

void MainWindow::printSummary(){
	textEdit->clear();
	textEdit->append("Statistics for loaded file: ");
	textEdit->append(QString("Total documents+queries: %1").arg(fd->pd->data_stats.num_queries));
	textEdit->append(QString("Unique queries: %1").arg(fd->pd->data_stats.uniq_queries.size()));
	textEdit->append(QString("Number of unique docs: %1").arg(fd->pd->data_stats.uniq_docs.size()));
	textEdit->append(QString("Total relevant queries: %1").arg(fd->pd->data_stats.rev_queries));
	double sum = std::accumulate(fd->pd->data_stats.rel_per_query.begin(), fd->pd->data_stats.rel_per_query.end(), 0.0);
	double avg_doc= sum/fd->pd->data_stats.uniq_queries.size();
	textEdit->append(QString("Average relevant docs per query: %1").arg(avg_doc));
	textEdit->append(QString("Number of features per query: %1").arg(fd->pd->data_stats.num_feats));
}

void MainWindow::printResults(QString test_id){
	textEdit->clear();
	QString svmrank_id = "SVM Rank";
	QString lrrank_id = "LR Rank";
	QString lr_id = "Log Regression";

	textEdit->append(QString("Test set results from: "+ test_id));

	if(test_id == lr_id){
		QString qstr = QString::number(lr_eval->acc);
		textEdit->append(QString("Accuracy: " + qstr));
		QString qstr_map = QString::number(lr_eval->out_res->total_map);
		textEdit->append(QString("Mean Avg Precision: " + qstr_map));
		textEdit->append(QString("Run time (in seconds): " + QString::number(lr_time)));
	}

	else if(test_id == lrrank_id){
		QString qstr_pwacc = QString::number(linrank_eval->pairwise_acc);
		textEdit->append(QString("Pairwise Accuracy: " + qstr_pwacc));
		QString qstr_map = QString::number(linrank_eval->out_res->total_map);
		textEdit->append(QString("Mean Avg Precision: " + qstr_map));
		QString qstr_ndcg = QString::number(linrank_eval->ndcg_acc);
		textEdit->append(QString("Mean NDCG: " + qstr_ndcg));
		textEdit->append(QString("Run time (in seconds): " + QString::number(lrrank_time)));

	}

	else if(test_id == svmrank_id){
		QString qstr_pwacc = QString::number(svmrank_eval->pairwise_acc);
		textEdit->append(QString("Pairwise Accuracy: " + qstr_pwacc));
		QString qstr_map = QString::number(svmrank_eval->out_res->total_map);
		textEdit->append(QString("Mean Avg Precision: " + qstr_map));
		QString qstr_ndcg = QString::number(svmrank_eval->ndcg_acc);
		textEdit->append(QString("Mean NDCG: " + qstr_ndcg));
		textEdit->append(QString("Run time (in seconds): " + QString::number(svm_time)));

	}

	else{
		textEdit->append(QString("Run time (in seconds) for Log Reg: " + QString::number(lr_time)));
		textEdit->append(QString("Run time (in seconds) for lin SVM: " + QString::number(lrrank_time)));
		textEdit->append(QString("Run time (in seconds) for non-Lin SVM: " + QString::number(svm_time)));
	}
}

void MainWindow::plot_inputrelev(){
	if (!fd){
		QMessageBox::critical(this, "Error", "Please load an appropriate dataset !");
	}
	else{
		plotRelevPD();
		 std::vector<double> rel_per_query(fd->pd->data_stats.rel_per_query.begin(), fd->pd->data_stats.rel_per_query.end());
		double sum = std::accumulate(fd->pd->data_stats.rel_per_query.begin(), fd->pd->data_stats.rel_per_query.end(), 0.0);
		double avg_doc= sum/fd->pd->data_stats.uniq_queries.size();
		plot_maptable(rel_per_query, avg_doc, 1);

	}
}

void MainWindow::plot_weights_lr(){
	if (!fd){
		QMessageBox::critical(this, "Error", "Please load an appropriate dataset !");
	}
	else if(!lr_eval){
		QMessageBox::critical(this, "Error", "Please train the data with desired method !");
	}
	else{
		const char *model_file_name = "saved_model.model";
		linear_model *train_model;
		train_model =load_model(model_file_name);
		plot_weights(train_model->w);
	}
}

void MainWindow::plot_weights_lrrank(){
	if (!fd){
		QMessageBox::critical(this, "Error", "Please load an appropriate dataset !");
	}
	else if(!linrank_eval){
		QMessageBox::critical(this, "Error", "Please train the data with desired method !");
	}
	else{
		const char *model_file_name = "saved_lrrank_model.model";
		linear_model *train_model;
		train_model =load_model(model_file_name);
		plot_weights(train_model->w);
	}
}

void MainWindow::plot_weights(double *w){
	customPlot->clearGraphs();
	if(relevPd_plot){
		customPlot->removePlottable(relevPd_plot);
	}

	if(lr_bar && lrrank_bar && svm_bar){
		customPlot->removePlottable(lr_bar);
		customPlot->removePlottable(lrrank_bar);
		customPlot->removePlottable(svm_bar);
	}

	relevPd_plot = new QCPBars(customPlot->xAxis, customPlot->yAxis);

	customPlot->show();
	customPlot->addPlottable(relevPd_plot);


	QPen pen;
	pen.setWidthF(1.2);
	relevPd_plot->setPen(pen);
	relevPd_plot->setBrush(QColor(255, 131, 0, 50));

	QVector<double> ticks;
	QVector<double> valueData;
	QVector<QString> labels;

	for(int i =0; i<pd_test->data_stats.num_feats; i++){
		ticks.push_back(i+1);
		valueData.push_back(w[i]);
	}

	customPlot->xAxis->setAutoTicks(false);
	customPlot->xAxis->setAutoTickLabels(false);
	customPlot->xAxis->setTickVector(ticks);
	customPlot->xAxis->setTickVectorLabels(labels);
	customPlot->xAxis->setTickLabelRotation(-60);
	customPlot->yAxis->setLabel("Learned weight values");

	relevPd_plot->setData(ticks, valueData);
	customPlot->legend->setVisible(false);

	//Plot it
	customPlot->rescaleAxes();
	customPlot->replot();
}

void MainWindow::plot_lrmap(){
	if (!fd){
		QMessageBox::critical(this, "Error", "Please load an appropriate dataset !");
	}
	else if(!lr_eval){
		QMessageBox::critical(this, "Error", "Please train the data with desired method !");
	}
	else{
		plot_maptable(lr_eval->out_res->map_vals_raw, lr_eval->out_res->total_map,2);
		plot_map(lr_eval->out_res->map_vals_raw);
	}
}

void MainWindow::plot_lrrankmap(){
	if (!fd){
		QMessageBox::critical(this, "Error", "Please load an appropriate dataset !");
	}
	else if(!linrank_eval){
		QMessageBox::critical(this, "Error", "Please train the data with desired method !");
	}
	else{
		plot_maptable(linrank_eval->out_res->map_vals_raw, linrank_eval->out_res->total_map, 3);
		plot_map(linrank_eval->out_res->map_vals_raw);
	}
}

void MainWindow::plot_svmrankmap(){
	if (!fd){
		QMessageBox::critical(this, "Error", "Please load an appropriate dataset !");
	}
	else if(!svmrank_eval){
		QMessageBox::critical(this, "Error", "Please train the data with desired method !");
	}
	else{
		plot_maptable(svmrank_eval->out_res->map_vals_raw, svmrank_eval->out_res->total_map, 4);
		plot_map(svmrank_eval->out_res->map_vals_raw);
	}
}


void MainWindow::plot_map_all(){
	customPlot->clearGraphs();
	customPlot->show();
	if(relevPd_plot){
		customPlot->removePlottable(relevPd_plot);
	}

	if(lr_bar && lrrank_bar && svm_bar){
		customPlot->removePlottable(lr_bar);
		customPlot->removePlottable(lrrank_bar);
		customPlot->removePlottable(svm_bar);
	}

	QCPBarsGroup *group = new QCPBarsGroup(customPlot);
	lr_bar = new QCPBars(customPlot->xAxis, customPlot->yAxis);
	lrrank_bar = new QCPBars(customPlot->xAxis, customPlot->yAxis);
	svm_bar = new QCPBars(customPlot->xAxis, customPlot->yAxis);
	group->append(lr_bar);
	group->append(lrrank_bar);
	group->append(svm_bar);


	customPlot->addPlottable(lrrank_bar);
	customPlot->addPlottable(svm_bar);
	customPlot->addPlottable(lr_bar);
	// set names and colors:
	QPen pen;
	pen.setWidthF(1.2);
	lr_bar->setName("Log Reg");
	pen.setColor(QColor(255, 131, 0));
	lr_bar->setPen(pen);
	lr_bar->setBrush(QColor(255, 131, 0, 50));
	lr_bar->setWidth(0.15);
	lrrank_bar->setName("LinSVM");
	pen.setColor(QColor(1, 92, 191));
	lrrank_bar->setPen(pen);
	lrrank_bar->setBrush(QColor(1, 92, 191, 50));
	lrrank_bar->setWidth(0.15);
	svm_bar->setName("non-LinSVM");
	pen.setColor(QColor(150, 222, 0));
	svm_bar->setPen(pen);
	svm_bar->setBrush(QColor(150, 222, 0, 70));
	svm_bar->setWidth(0.15);
	QVector<double> ticks;
	QVector<QString> labels;


	for (std::vector<std::string>::iterator it = fd->pd->data_stats.uniq_queries.begin() ; it != fd->pd->data_stats.uniq_queries.end(); ++it){
		QString qstr = QString::fromStdString(*it);
		labels << qstr;
	}

	for(int i = 0; i<pd_test->data_stats.uniq_queries.size(); i++){
		ticks<< i+1;
	}

	customPlot->xAxis->setAutoTicks(false);
	customPlot->xAxis->setAutoTickLabels(false);
	customPlot->xAxis->setTickVector(ticks);
	customPlot->xAxis->setTickVectorLabels(labels);
	customPlot->xAxis->setTickLabelRotation(60);
	customPlot->xAxis->setSubTickCount(0);
	customPlot->xAxis->grid()->setVisible(true);

	// prepare y axis:
	customPlot->yAxis->setLabel("MAP vals for \nLR, lin-SVM, non-linSVM");
	customPlot->yAxis->grid()->setSubGridVisible(true);
	QPen gridPen;
	gridPen.setStyle(Qt::SolidLine);
	gridPen.setColor(QColor(0, 0, 0, 25));
	customPlot->yAxis->grid()->setPen(gridPen);
	gridPen.setStyle(Qt::DotLine);
	customPlot->yAxis->grid()->setSubGridPen(gridPen);

	// Add data:
	QVector<double> lr_data, lrrank_data, svm_data;
	for(int i=0; i<pd_test->data_stats.uniq_queries.size(); i++){
		lr_data << lr_eval->out_res->map_vals_raw[i];
		lrrank_data << linrank_eval->out_res->map_vals_raw[i];
		svm_data << svmrank_eval->out_res->map_vals_raw[i];
	}

	lr_bar->setData(ticks, lr_data);
	lrrank_bar->setData(ticks, lrrank_data);
	svm_bar->setData(ticks, svm_data);
	lr_bar->setBarsGroup(group);
	lrrank_bar->setBarsGroup(group);
	svm_bar->setBarsGroup(group);

	// setup legend:
	customPlot->legend->setVisible(true);
	customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop);
	customPlot->legend->setBrush(QColor(255, 255, 255, 200));
	QPen legendPen;
	legendPen.setColor(QColor(130, 130, 130, 200));
	customPlot->legend->setBorderPen(legendPen);
	QFont legendFont = font();
	legendFont.setPointSize(10);
	customPlot->legend->setFont(legendFont);
	customPlot->replot();
}

void MainWindow::plot_map(std::vector<double> map_vals){
	/// prepare axes:
	customPlot->show();
	customPlot->clearGraphs();

	if(relevPd_plot){
		customPlot->removePlottable(relevPd_plot);
	}

	relevPd_plot = new QCPBars(customPlot->xAxis, customPlot->yAxis);

	if(lr_bar && lrrank_bar && svm_bar){
		customPlot->removePlottable(lr_bar);
		customPlot->removePlottable(lrrank_bar);
		customPlot->removePlottable(svm_bar);
	}

	customPlot->addPlottable(relevPd_plot);

	QPen pen;
	pen.setWidthF(1.2);
	relevPd_plot->setPen(pen);
	relevPd_plot->setBrush(QColor(255, 131, 0, 50));
	//map_vals_total
	QVector<double> ticks;
	QVector<QString> labels;

	int i = 1;
	for (std::vector<std::string>::iterator it = fd->pd->data_stats.uniq_queries.begin() ; it != fd->pd->data_stats.uniq_queries.end(); ++it){
		QString qstr = QString::fromStdString(*it);
		labels.push_back(qstr);
		ticks.push_back(i);
		i++;
	}

	QVector<double> valueData;

	std::vector<double>::iterator dit;
	i =0;
	for(dit= map_vals.begin(); dit !=map_vals.end(); dit++, i++){
		ticks.push_back(i+1);
		valueData.push_back(*dit);
	}



	customPlot->xAxis->setAutoTicks(false);
	customPlot->xAxis->setAutoTickLabels(false);
	customPlot->xAxis->setTickVector(ticks);
	customPlot->xAxis->setTickVectorLabels(labels);
	customPlot->xAxis->setTickLabelRotation(-60);
	customPlot->yAxis->setLabel("Average precision per query");

	relevPd_plot->setData(ticks, valueData);
	customPlot->legend->setVisible(false);
	//Plot it
	customPlot->rescaleAxes();
	customPlot->replot();

}
void MainWindow::plot_maptable(std::vector<double> map_vals, double total_map, int which_table){
	if (!fd){
		QMessageBox::critical(this, "Error", "Please load and train an appropriate dataset!");
	}
	else{
		mapTable->clear();
		mapTable->setRowCount(map_vals.size()+1);
		QStringList vlabels;

		if(which_table ==1){
			mapTable->setColumnCount(1);
			vlabels << tr("rel docs");
		}
		else{
			mapTable->setColumnCount(3);
			for(int column = 0; column < map_vals.size(); column++) {
				QString text1 = QString::number(pd_test->data_stats.rel_per_query[column]);
				QString text2 = QString::number(pd_test->data_stats.doc_per_query[column]);
				QTableWidgetItem *item1 = new QTableWidgetItem(text1);
				QTableWidgetItem *item2 = new QTableWidgetItem(text2);
				mapTable->setItem(column, 1, item1);
				mapTable->setItem(column, 2, item2);
				vlabels << tr("MAP val")<< tr("rel docs") <<  tr("total docs");
			}
		}


		QStringList hlabels;

		for (std::vector<std::string>::iterator it = fd->pd->data_stats.uniq_queries.begin() ; it != fd->pd->data_stats.uniq_queries.end(); ++it){
			QString qstr = QString::fromStdString(*it);
			hlabels << qstr;
		}
		hlabels << tr("Avg");

		//
		for(int column = 0; column < map_vals.size(); column++) {
			QString text1 = QString::number(map_vals[column]);
			QTableWidgetItem *item1 = new QTableWidgetItem(text1);
			mapTable->setItem(column, 0, item1);
		}

		mapTable->setItem(map_vals.size(), 0, new QTableWidgetItem(QString::number(total_map)));
		mapTable->setHorizontalHeaderLabels(vlabels);
		mapTable->setVerticalHeaderLabels(hlabels);
		mapTable->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
		mapTable->verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);
	}
}

void MainWindow::plot_maptable(){
	if (!fd){
		QMessageBox::critical(this, "Error", "Please load an appropriate dataset !");
	}
	else if(!lr_eval || !linrank_eval || !svmrank_eval){
		QMessageBox::critical(this, "Error", "Please train data on all functions !");
	}
	else{
		mapTable->clear();
		mapTable->setRowCount(fd->pd->data_stats.uniq_queries.size()+1);
		mapTable->setColumnCount(3);
		QStringList hlabels;
		for (std::vector<std::string>::iterator it = fd->pd->data_stats.uniq_queries.begin() ; it != fd->pd->data_stats.uniq_queries.end(); ++it){
			QString qstr = QString::fromStdString(*it);
			hlabels << qstr;
		}

		hlabels << tr("Avg");

		for(int column = 0; column < fd->pd->data_stats.uniq_queries.size(); column++) {
			QString text1 = QString::number(lr_eval->out_res->map_vals_raw[column]);
			QString text2 = QString::number(linrank_eval->out_res->map_vals_raw[column]);
			QString text3 = QString::number(svmrank_eval->out_res->map_vals_raw[column]);
			QTableWidgetItem *item1 = new QTableWidgetItem(text1);
			QTableWidgetItem *item2 = new QTableWidgetItem(text2);
			QTableWidgetItem *item3 = new QTableWidgetItem(text3);
			mapTable->setItem(column, 0, item1);
			mapTable->setItem(column, 1, item2);
			mapTable->setItem(column, 2, item3);
		}

		mapTable->setItem(fd->pd->data_stats.uniq_queries.size(), 0, new QTableWidgetItem(QString::number(lr_eval->out_res->total_map)));
		mapTable->setItem(fd->pd->data_stats.uniq_queries.size(), 1, new QTableWidgetItem(QString::number(linrank_eval->out_res->total_map)));
		mapTable->setItem(fd->pd->data_stats.uniq_queries.size(), 2, new QTableWidgetItem(QString::number(svmrank_eval->out_res->total_map)));
		QStringList vlabels;
		vlabels << tr("LR")<< tr("LinSVM")<< tr("nlSVM");
		mapTable->setHorizontalHeaderLabels(vlabels);
		mapTable->setVerticalHeaderLabels(hlabels);
		mapTable->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
		mapTable->verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);
	}

	plot_map_all();
}

void MainWindow::lrVSlrrank(){
	if(!lr_eval || !linrank_eval){
		QMessageBox::critical(this, "Error", "Please train data on both logistic regression and linear svm rank first !");
	}
	else{
		apply_zerotest(1);
	}

}

void MainWindow::lrVSsvmrank(){
	if(!lr_eval || !svmrank_eval){
		QMessageBox::critical(this, "Error", "Please train data on both logistic regression and non-linear svm rank first !");
	}
	else{
		apply_zerotest(2);
	}
}

void MainWindow::lrrankVSsvmrank(){
	if(!linrank_eval || !svmrank_eval){
		QMessageBox::critical(this, "Error", "Please train data on both linear svm rank and non-linear svm rank first !");
	}
	else{
		apply_zerotest(3);
	}
}

void MainWindow::apply_zerotest(int which_test){
	qDebug() << "apply_zerotest" ;
	double z_val, sign_prob;
	std::vector<double> raw_map1, raw_map2;
	QStringList vlabels;
	Compmethods *comp = new Compmethods;

	if (which_test==1){
		comp->sign_test(lr_eval->out_res, linrank_eval->out_res);
		comp->ztest(lr_eval->out_res, linrank_eval->out_res);
		z_val = comp->ztest_val;
		sign_prob = comp->final_prob;
		raw_map1 = lr_eval->out_res->map_vals_raw;
		raw_map2 = linrank_eval->out_res->map_vals_raw;
		vlabels << tr("LR")<< tr("LinSVM")<< tr("Diff");
	}

	else if (which_test==2){
		comp->sign_test(lr_eval->out_res, svmrank_eval->out_res);
		comp->ztest(lr_eval->out_res, svmrank_eval->out_res);
		z_val = comp->ztest_val;
		sign_prob = comp->final_prob;
		raw_map1 = lr_eval->out_res->map_vals_raw;
		raw_map2 = svmrank_eval->out_res->map_vals_raw;
		vlabels << tr("LR")<< tr("non-LinSVM")<< tr("Diff");
	}

	else if (which_test==3){
		comp->sign_test(linrank_eval->out_res, svmrank_eval->out_res);
		comp->ztest(linrank_eval->out_res, svmrank_eval->out_res);
		z_val = comp->ztest_val;
		sign_prob = comp->final_prob;
		raw_map1 = linrank_eval->out_res->map_vals_raw;
		raw_map2 = svmrank_eval->out_res->map_vals_raw;
		vlabels << tr("LinSVM")<< tr("non-LinSVM")<< tr("Diff");
	}

	mapTable->clear();
	mapTable->setRowCount(pd_test->data_stats.uniq_queries.size()+1);
	mapTable->setColumnCount(3);
	QStringList labels;
	for(int i = 0; i<pd_test->data_stats.uniq_queries.size(); i++){
		labels << QString::number(i+1);
	}
	labels << tr("Avg MAP");
	int column = 0;
	for(std::vector<double>::iterator it= comp->sign_test_vec.begin(); it!= comp->sign_test_vec.end(); it++, column++){
		QString text1 = QString::number(raw_map1[column]);
		QString text2 = QString::number(raw_map2[column]);
		QString text3 = QString::number(*it);
		QTableWidgetItem *item1 = new QTableWidgetItem(text1);
		QTableWidgetItem *item2 = new QTableWidgetItem(text2);
		QTableWidgetItem *item3 = new QTableWidgetItem(text3);
		mapTable->setItem(column, 0, item1);
		mapTable->setItem(column, 1, item2);
		mapTable->setItem(column, 2, item3);
	}

	mapTable->setHorizontalHeaderLabels(vlabels);
	mapTable->setVerticalHeaderLabels(labels);
	mapTable->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
	mapTable->verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);

	textEdit->clear();
	textEdit->append("Results of analysis using sign test: ");
	textEdit->append(QString::number(sign_prob));
	textEdit->append("Results of analysis using z-test: ");
	textEdit->append(QString::number(z_val));
	if (z_val>1.96){
		textEdit->append("A z-score greater than 1.96 suggests with 95% confidence that the observed results "
				"come from different distributions. This means that the is a great chance"
				"that the training algorithms for one method are superior to another.");
	}
	else{
		textEdit->append("A z-score less than 1.96 means that the hypothesis that the observed results "
				"come from different distributions cannot be rejected. This means that there is little chance"
				"that the training algorithms for one method are superior to another.");
	}
}
