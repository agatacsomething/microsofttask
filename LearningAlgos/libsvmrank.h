/**
 * Class: LIBSVMRANK
 * Description: Computation of svmrank model for input data. For now default is set to RBF kernel
 * Input: Parsed training or testing data.
 * 		ParsedData *pd,
 * Output: Learned model, pairwise accuracy, mean ndcg, queries ranked
 * 		svm_model *train_model
 * 		std::vector<double> raw_ranks2;
 * 		double pairwise_acc;
 * 		double ndcg_acc;
 * Functions:
 * 		Functions: Two main functions, one to train data (output-train_model), and one to test data (output - ranked results, pairwise acc, ndcg)
 * 		void libsvmrank_train(ParsedData *pd);
 * 		void libsvmrank_test(ParsedData *pd, svm_model *train_model);
 * Sample Usage:
 * 		ParsedData *pd_train; //preloaded train data
 * 		linrank_train(pd_train);
 *
 * 		ParsedData *pd_test;  //preloaded test data
 * 		svm_model *train_model; // variable in which model is written to
 * 		libsvmrank_test(pd_test, train_model);
 */

#ifndef LIBSVMRANK_H
#define LIBSVMRANK_H

#include "preprocessdata.h"
#include "svm.h"

class Libsvmrank{
public:
	std::vector<double> raw_ranks2;
	double pairwise_acc;
	double ndcg_acc;

	Libsvmrank();
	void libsvmrank_train(ParsedData pd);
	void libsvmrank_test(ParsedData pd, svm_model *train_model);
private:
	svm_parameter param;

	void init_params();
	void get_traindata(ParsedData *pd, svm_problem *prob);

	void get_featurevec(ParsedData *pd, svm_problem *prob);
	void get_queryvec(ParsedData *pd, svm_problem *prob);
	void get_queryvec(ParsedData *pd, int *qvec);

};

#endif
