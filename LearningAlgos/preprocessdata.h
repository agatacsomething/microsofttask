/**
 * Class: PreprocessData
 * Description: Iterates through a given file and generates an output struct of the
 * 		parsed data.
 * Sample Usage:
 * 		PreprocessData pd;
 * 		std::string filename = "file.txt";
 * 		pd.loadDataFile(filename);
 * Output:
 * 		parsed_data - a vector of the parse data from the file
 * 		data_stats - a struct of the parsed data statistics
 * Secondary function: contains a static function "SplitData" which can be used to further
 * 		split the data. This is meant for cases in which training and testing data
 * 		are to be subsets of the orginal dataset.
 * Sample Usage:
 * 		PreprocessData *pd; // data should be already loaded in this instance
 * 		ParsedData *pd_1 = new ParsedData;
 * 		ParsedData *pd_2 = new ParsedData;
 * 		int split_flag = 2; (default)
 * 		PreprocessData::SplitData(pd->parsed_data, pd_1, pd_2, int split_flag);
 */

#ifndef PREPROCESSDATA_H
#define PREPROCESSDATA_H

#include <string>
#include <vector>
#include <numeric>

struct DataStats{
	int num_queries;
	int rev_queries;
	int num_feats;
	std::vector<std::string> uniq_queries;
	std::vector<std::string> uniq_docs;
	std::vector<int> rel_per_query;
	std::vector<int> doc_per_query;
};

struct ParsedLine{
	int relev;
	std::string qid;
	std::vector<double> feats;
	std::string docid;
};

struct ParsedData{
	DataStats data_stats;
	std::vector<ParsedLine> parsed_data;
};

class PreprocessData
{
public:
	ParsedData *pd;

	PreprocessData();
	void loadDataFile(std::string fn);

	static void SplitData(std::vector<ParsedLine> pd, ParsedData *pd1, ParsedData *pd2, int split_flag);
	static void get_stats(ParsedData *parsed);
	static int getnumqueries(std::vector<ParsedLine> parse_d);
	static int getnumrelqueries(std::vector<ParsedLine> parse_d);
	static int getfeatsperquery(std::vector<ParsedLine> parse_d);
	static void getuniquequeries(ParsedData *parsed);
	static void getuniquedocs(ParsedData *parsed);
	static void getreldocsperquery(ParsedData *parsed);
	static void getdocsperquery(ParsedData *parsed);

private:
	void parse_line(std::string fn);
	void parse_qid(std::string qidstr, ParsedLine &parsed_data_line);
	void parse_docid(std::string qidstr, ParsedLine &parsed_data_line);
	void parse_features(std::vector<std::string> strs, ParsedLine &parsed_data_line);
	void printstrs(std::vector<std::string> strs);
};

#endif
