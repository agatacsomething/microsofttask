#include "crossfilefuncts.h"
#include <iostream>
#include <algorithm>
#include <math.h>

void CrossfileFuncts::GetSparseFeatLength(std::vector<double> &features, int &num_l){
	for (int k=0; k<features.size(); k++){
		if(features[k]>0){
			num_l++;
		}
	}
}

void CrossfileFuncts::MakeSparseFeatures (std::vector<double> &features, feature_node *x_space){
	int j=0;
	for (int k=0; k<features.size(); k++){
		if(features[k]>0){
			x_space[j].index = j+1;
			x_space[j].value = features[k];
			j++;
		}
	}
	x_space[j].index = -1;
}

void CrossfileFuncts::MakeSparseFeatures (std::vector<double> &features, svm_node *x_space){
	int j=0;
	for (int k=0; k<features.size(); k++){
		if(features[k]>0){
			x_space[j].index = j+1;
			x_space[j].value = features[k];
			j++;
		}
	}
	x_space[j].index = -1;
}

double * CrossfileFuncts::get_yvec(std::vector<ParsedLine> *pd, int which_train){
	double * y_test = new double[pd->size()];
	int i=0;
	for(std::vector<ParsedLine>::iterator it= pd->begin(); it!=pd->end(); it++, i++){
		ParsedLine sample = *it;

		if(sample.relev ==-1){
			if (which_train==1){
				y_test[i]= 0;
			}
			else{
				y_test[i]= (int)round(sample.relev);
			}
		}
		else{
			y_test[i]= (int)round(sample.relev);
		}
	}
	return y_test;
}

int * CrossfileFuncts::get_queryvec(ParsedData *pd){
	int k =0;
	int *query = new int[pd->parsed_data.size()];
	for (std::vector<ParsedLine>::iterator it = pd->parsed_data.begin(); it != pd->parsed_data.end(); it++, k++){
		ParsedLine temp_vec = *it;
		std::string temp_str = temp_vec.qid;
		int count=0;
		for (std::vector<std::string>::iterator it2 = pd->data_stats.uniq_queries.begin() ; it2 != pd->data_stats.uniq_queries.end(); it2++){
			count++;
			if(temp_str.compare(*it2) == 0){
				query[k]=count;
			}
		}
	}
	return query;
}
