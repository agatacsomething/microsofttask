/**
 * Class: LogReg
 * Description: Computation of logistic regression model for input data
 * Input: Parsed training or testing data.
 * 		ParsedData *pd,
 * Output: Learned model, pairwise accuracy, mean ndcg, queries ranked.
 * 		linear_model *train_model
 * 		std::vector<double> raw_ranks2;
 * 		double pairwise_acc;
 * 		double ndcg_acc;
 * Functions:
 * 		Functions: Two main functions, one to train data (output-train_model), and one to test data (output - ranked results, pairwise acc, ndcg)
 * 		void logreg_train(ParsedData *pd);
 * 		void logreg_test(ParsedData *pd, linear_model *train_model);
 * Sample Usage:
 * 		ParsedData *pd_train; //preloaded train data
 * 		logreg_train(pd_train);
 *
 * 		ParsedData *pd_test;  //preloaded test data
 * 		linear_model *train_model; // variable in which model is written to
 * 		logreg_test(pd_test, train_model);
 */

#ifndef LOGREG_H
#define LOGREG_H

#include "linear.h"
#include "preprocessdata.h"
#include "evalresults.h"
#include <math.h>


class LogReg{
public:
	LogReg();
	void logreg_train(ParsedData pd);
	void logreg_test(ParsedData pd, linear_model *train_model);

	double accuracy;
	std::vector<double> raw_ranks2;

private:
	void init_params();
	void get_traindata(ParsedData *pd, problem *prob);
	void get_featurevec(ParsedData *pd, problem *prob);

	parameter param;
	std::vector<int> true_label;

};

#endif
