/**
 * Class: Crossfilefunct
 * Description: Class of static functions used across training and testing of algorithms
 * Functions:
 * 		MakeSparseFeatures
 * 			Description:  makes features sparse - removing zeros - as needed by liblinear and libsvm to train and test data
 * 			Sample Usage:
 * 				std::vector<double> features //vector of features
 * 				node *x_space 				 //either svm_node or feature_node, which stores new form of data
 * 				Crossfilefunct::MakeSparseFeatures(features, node *x_space)
* 		GetSparseFeatLength
 * 			Description:  returns the length of the non-sparse features, used to set up feature nodes of correct lengths for training and testing
 * 			Sample Usage:
 * 				std::vector<double> features //vector of features
 * 				int num_l; 				 //stores number of elements
 * 				Crossfilefunct::GetSparseFeatLength(features, num_l);
 * 		get_yvec
 * 			Description:  returns an array of labels for each query
 * 			Sample Usage:
 * 				std::vector<ParsedLine> *pd	 	 //vector of preloaded features
 * 				int which_train=1; 				 //Value 1 re-assigns negative class labels to 0 as opposed to -1
 * 				double *  ytest = Crossfilefunct::get_yvec(pd, 1);
 * 		get_queryvec
 * 			Description:  returns an integer array of unique query id labels, instead of the default string labels
 * 			Sample Usage:
 * 				ParsedData *pd	 	 			 //vector of preloaded data
 * 				int *  ytest = Crossfilefunct::get_queryvec(pd);
*/

#ifndef CROSSFILEFUNCTS_H
#define CROSSFILEFUNCTS_H

#include "linear.h"
#include "svm.h"
#include <vector>
#include "evalresults.h"

typedef std::vector < std::vector<double> > FeatureVec;

class CrossfileFuncts{

public:
	static void MakeSparseFeatures (std::vector<double> &features, svm_node *x_space);
	static void MakeSparseFeatures (std::vector<double> &features, feature_node *x_space);
	static void GetSparseFeatLength(std::vector<double> &features, int &num_l);
	static double * get_yvec(std::vector<ParsedLine> *pd, int which_train);
	static int * get_queryvec(ParsedData *pd);
};

#endif
