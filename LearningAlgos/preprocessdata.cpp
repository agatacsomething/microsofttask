#include "preprocessdata.h"
#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>


PreprocessData :: PreprocessData(): pd(NULL){
}

/**
 * Function: loadDataFile
 * Description: Iterates through file line by line and parses data.
 * Sample Usage:
 * 		std::string sample_file = "testfile.txt";
 * 		loadDataFile(sample_file);
 * Output:
 * 		none
 */
void PreprocessData :: loadDataFile(std::string fn){
	std::ifstream file(fn);
	std::string str;
	pd = new ParsedData;
	while (std::getline(file, str))
	{
		parse_line(str);
	}

	get_stats(pd);
}

/**
 * Function: parse_line
 * Description: Splits a given line of file by spaces into assumed variables.
 * Sample Usage:
 * 		std::string sample_string = "1 qid:WT04-170 1:0.033761 2:0.031250 #docid = G06-90-0596202
 * 		parse_line(sample_string);
 * Output:
 * 		void function, but stores results of parsing in public variable parsed_data
 */
void PreprocessData :: parse_line(std::string str){
	ParsedLine parsed_data_line;

	std::vector<std::string> strs;
	boost::split(strs,str,boost::is_any_of(" "),boost::token_compress_on);

	//Parse doc-id
	parse_docid(strs.back(), parsed_data_line);
	strs.pop_back();
	strs.pop_back();
	strs.pop_back();

	//Once doc-id taken out, reverse list
	std::reverse(strs.begin(), strs.end());

	//Parse relevance id
	int temp_int =std::stoi( strs.back());
	if (temp_int ==0){
		temp_int =-1;
	}
	parsed_data_line.relev=temp_int;
	strs.pop_back();

	//Parse qid
	parse_qid(strs.back(), parsed_data_line);
	strs.pop_back();

	//Parse features
	parse_features(strs, parsed_data_line);

	//Push struct into main data vector
	pd->parsed_data.push_back(parsed_data_line);

}

/**
 * Function: printstrs
 * Description: Printing function, used for debugging
 * Sample Usage:
 * 		std::vector<std::string> vec_of_strs;
 * 		printstrs(vec_of_strs);
 * Output:
 * 		prints to console the vector of strings
 */
void PreprocessData ::printstrs(std::vector<std::string> strs){
    for(std::vector<std::string>::iterator it = strs.begin();it!=strs.end();++it){
        std::cout<<*it<<std::endl;
    }
}

void PreprocessData ::parse_qid(std::string qidstr, ParsedLine &parsed_data_line){
	std::vector<std::string> strs;
	boost::split(strs,qidstr,boost::is_any_of(":"),boost::token_compress_on);
	parsed_data_line.qid = strs.back();
}

void PreprocessData ::parse_docid(std::string docidstr, ParsedLine &parsed_data_line){
	std::vector<std::string> strs;
	boost::split(strs,docidstr,boost::is_any_of("="),boost::token_compress_on);
	parsed_data_line.docid= strs.back();
}

void PreprocessData ::parse_features(std::vector<std::string> strs, ParsedLine &parsed_data_line){

	std::vector<double> feat_vals;

	for(std::vector<std::string>::iterator it= strs.begin() ; it != strs.end(); ++it){
		std::vector<std::string> featstr;
		boost::split(featstr,*it,boost::is_any_of(":"),boost::token_compress_on);
		double temp_val = std::atof(featstr.back().c_str());
		feat_vals.push_back(temp_val);


	}

	std::reverse(feat_vals.begin(), feat_vals.end());
	parsed_data_line.feats= feat_vals;
}

void PreprocessData::get_stats(ParsedData *parsed){
	//Number of total queries
	parsed->data_stats.num_queries=getnumqueries(parsed->parsed_data);

	//Number of total queries marked as relevant
	parsed->data_stats.rev_queries =getnumrelqueries(parsed->parsed_data);

	//Number of features per query
	parsed->data_stats.num_feats = getfeatsperquery(parsed->parsed_data);

	//Unique documents
	getuniquedocs(parsed);

	//Unique queries
	getuniquequeries(parsed);

	///Documents per query
	getreldocsperquery(parsed);
	getdocsperquery(parsed);
}

int PreprocessData::getnumqueries(std::vector<ParsedLine> parse_d){
	return parse_d.size();
}

int PreprocessData::getnumrelqueries(std::vector<ParsedLine> parse_d){
	int total_pos = 0;
	for(std::vector<ParsedLine>::iterator it = parse_d.begin(); it!=parse_d.end(); it++){
		ParsedLine temp_vec = *it;
		if(temp_vec.relev>0){
			total_pos++;
		}
	}
	return total_pos;
}

int PreprocessData::getfeatsperquery(std::vector<ParsedLine> parse_d){
	ParsedLine s = parse_d.back();
	return s.feats.size();
}

bool compstrings (std::string i, std::string j) {
  return (i.compare(j));
}

void PreprocessData::getuniquequeries(ParsedData *parsed){
	std::vector<std::string> temp_qids;
	for(std::vector<ParsedLine>::iterator it = parsed->parsed_data.begin(); it != parsed->parsed_data.end(); it++){
		ParsedLine temp_vec = *it;
		temp_qids.push_back(temp_vec.qid);
	}

	std::sort(temp_qids.begin(), temp_qids.end());
	temp_qids.erase( std::unique( temp_qids.begin(), temp_qids.end() ), temp_qids.end() );
	parsed->data_stats.uniq_queries = temp_qids;
}

void PreprocessData::getuniquedocs(ParsedData *parsed){
	std::vector<std::string> temp_docs;
	for(std::vector<ParsedLine>::iterator it = parsed->parsed_data.begin(); it != parsed->parsed_data.end(); it++){
		ParsedLine temp_vec = *it;
		temp_docs.push_back(temp_vec.docid);
	}
	temp_docs.erase( std::unique( temp_docs.begin(), temp_docs.end() ), temp_docs.end() );
	parsed->data_stats.uniq_docs = temp_docs;
}

void PreprocessData::getreldocsperquery(ParsedData *parsed){
	for (std::vector<std::string>::iterator it2 = parsed->data_stats.uniq_queries.begin() ; it2 != parsed->data_stats.uniq_queries.end(); it2++){
		int count=0;
		for (std::vector<ParsedLine>::iterator it = parsed->parsed_data.begin() ; it != parsed->parsed_data.end(); it++){
			ParsedLine temp_vec = *it;
			std::string temp_str = temp_vec.qid;

			if(temp_str.compare(*it2) == 0){
				if(temp_vec.relev>0){
					count ++;
				}
			}
		}
		parsed->data_stats.rel_per_query.push_back(count);
	}
}

void PreprocessData::getdocsperquery(ParsedData *parsed){
	for (std::vector<std::string>::iterator it2 = parsed->data_stats.uniq_queries.begin() ; it2 != parsed->data_stats.uniq_queries.end(); it2++){
		int count=0;
		for (std::vector<ParsedLine>::iterator it = parsed->parsed_data.begin() ; it != parsed->parsed_data.end(); it++){
			ParsedLine temp_vec = *it;
			std::string temp_str = temp_vec.qid;

			if(temp_str.compare(*it2) == 0){
					count ++;
			}
		}
		parsed->data_stats.doc_per_query.push_back(count);
	}
}

void PreprocessData::SplitData(std::vector<ParsedLine> pdin, ParsedData *pd1, ParsedData *pd2, int split_flag){
	if(split_flag ==1){
		//for now just grab first data
	}
	else if(split_flag==2){
		std::random_shuffle(pdin.begin(), pdin.end());
		int pd1_sz = 0.8*pdin.size();
		int pd2_sz = pdin.size()-pd1_sz;
		int i=0;
		for(std::vector<ParsedLine>::iterator it= pdin.begin(); it!= pdin.end(); it++, i++){
			if(i<pd1_sz){
				pd1->parsed_data.push_back(*it);
			}
			else{
				pd2->parsed_data.push_back(*it);
			}
		}
	}

	get_stats(pd1);
	get_stats(pd2);
}
