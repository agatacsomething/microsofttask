/**
 * Class: LINRANK
 * Description: Computation of linear svmrank model for input data
 * Input: Parsed training or testing data.
 * 		ParsedData *pd,
 * Output: Learned model, pairwise accuracy, mean ndcg, queries ranked.
 * 		linear_model *train_model
 * 		std::vector<double> raw_ranks2;
 * 		double pairwise_acc;
 * 		double ndcg_acc;
 * Functions:
 * 		Functions: Two main functions, one to train data (output-train_model), and one to test data (output - ranked results, pairwise acc, ndcg)
 * 		void linrank_train(ParsedData *pd);
 * 		void linrank_test(ParsedData *pd, linear_model *train_model);
 * Sample Usage:
 * 		ParsedData *pd_train; //preloaded train data
 * 		linrank_train(pd_train);
 *
 * 		ParsedData *pd_test;  //preloaded test data
 * 		linear_model *train_model; // variable in which model is written to
 * 		linrank_test(pd_test, train_model);
 */

#ifndef LINRANK_H
#define LINRANK_H

#include "preprocessdata.h"
#include "ranksvm.h"

class LinRank{
public:
	LinRank();
	void linrank_train(ParsedData pd);
	void linrank_test(ParsedData pd, linear_model *train_model);

	//output of test results
	//Rankresults *lrrank_res;
	std::vector<double> raw_ranks2;
	double pairwise_acc;
	double ndcg_acc;
private:

	void init_params();
	void get_traindata(ParsedData *pd, problem *prob);

	void get_featurevec(ParsedData *pd, problem *prob);
	//void get_queryvec(std::vector<ParsedData> pd, DataStats *data_stats, problem *prob);
	int* get_queryvec(ParsedData *pd);
	//void split_results(std::vector<double> 	raw_ranks2, ParsedData *pd);
	//void rank_results(std::vector<std::tuple<double, int, std::string>> quer_subset);
	//parameters
	parameter param;


};

#endif
