#include "libsvmrank.h"
#include <iostream>
#include "crossfilefuncts.h"

Libsvmrank:: Libsvmrank(){
	pairwise_acc=0;
	ndcg_acc=0;
}

void Libsvmrank::init_params(){
	param.svm_type = L2R_RANK;
	param.kernel_type = RBF;
	param.degree = 3;
	param.gamma = 0.5;
	param.coef0 = 0;
	param.cache_size = 1000;
	param.C = 0.001;
	param.eps = 0.001;
}

void Libsvmrank::libsvmrank_test(ParsedData pd, svm_model *train_model){
	double *dvec_t= new double[pd.parsed_data.size()];
	double * ivec_t = CrossfileFuncts::get_yvec(&pd.parsed_data,2);

	svm_node* testnode;

	//Change data struct to Libsvm format
	std::vector<int>pred_labels;

	int k =0;
	for(std::vector<ParsedLine>::iterator it= pd.parsed_data.begin(); it!=pd.parsed_data.end(); it++){
		ParsedLine sample = *it;
		std::vector<double> feat_vec = sample.feats;
		int num_l=0;
		CrossfileFuncts::GetSparseFeatLength(feat_vec, num_l);
		svm_node *x_space = new svm_node[num_l+1];
		CrossfileFuncts::MakeSparseFeatures (feat_vec, x_space);
		double prob_est;
		prob_est= svm_predict(train_model, x_space);
		dvec_t[k]=prob_est;
		raw_ranks2.push_back(prob_est);
		k++;
	}

	double *result = new double[2];
	int *qvec= CrossfileFuncts::get_queryvec(&pd);
	int n =pd.parsed_data.size();
	svm_eval_list(ivec_t,dvec_t,qvec,n,result);
	pairwise_acc = result[0]*100.0;
	ndcg_acc = result[1];
}



void Libsvmrank::libsvmrank_train(ParsedData pd){
	init_params();
	svm_model *train_model;
	svm_problem prob;
	svm_check_parameter(&prob,&param);
	get_traindata(&pd, &prob);
	train_model=svm_train(&prob,&param);
	const char *model_file_name = "saved_libsvmrank_model.model";
	int saved= svm_save_model(model_file_name, train_model);
}


void Libsvmrank::get_traindata(ParsedData *pd, svm_problem *prob){
	prob->l = pd->parsed_data.size();
	prob->n = pd->data_stats.num_feats;

	//Change data struct to Libsvm format
	double *y_test = CrossfileFuncts::get_yvec(&pd->parsed_data,2);
	prob->y = y_test;
	get_featurevec(pd, prob);
	int *query = CrossfileFuncts::get_queryvec(pd);
	prob->query = query;
}

void Libsvmrank::get_featurevec(ParsedData *pd, svm_problem *prob){
	svm_node** x = new svm_node *[prob->l];

	int i =0;
	for(std::vector<ParsedLine>::iterator it= pd->parsed_data.begin(); it!=pd->parsed_data.end(); it++, i++){
		ParsedLine sample = *it;
		std::vector<double> feat_vec = sample.feats;

		int num_l=0;
		CrossfileFuncts::GetSparseFeatLength(feat_vec, num_l);
		svm_node *x_space = new svm_node[num_l+1];
		CrossfileFuncts::MakeSparseFeatures (feat_vec, x_space);
		x[i] = x_space;
	}
	prob->x = x;
}
