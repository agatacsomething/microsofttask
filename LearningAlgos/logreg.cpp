#include "logreg.h"
#include <iostream>
#include "ranksvm.h"
#include "crossfilefuncts.h"

LogReg::LogReg(){
	accuracy= 0;
}

void LogReg::init_params(){
	param.solver_type=L2R_LR;
	param.eps= 0.0001;
	param.C=0.001;
	param.nr_weight=2;
	int weight_label[2];
	weight_label[0]=0;
	weight_label[1]=1;
	param.weight_label=weight_label;
	double weight[2];
	weight[0]=-100;
	weight[1]=1;
	param.weight=weight;
	param.p=0.00001;
}

void LogReg::logreg_test(ParsedData pd, linear_model *train_model){
	//Change data struct to Libsvm format
	double * test_labels = CrossfileFuncts::get_yvec(&pd.parsed_data,1);

	int k =0;
	int sum_1s=0;
	int acc_1=0;
	for(std::vector<ParsedLine>::iterator it= pd.parsed_data.begin(); it!=pd.parsed_data.end(); it++){
		ParsedLine sample = *it;
		std::vector<double> feat_vec = sample.feats;

		int num_l=0;
		CrossfileFuncts::GetSparseFeatLength(feat_vec, num_l);
		feature_node *x_space = new feature_node[num_l+1];
		CrossfileFuncts::MakeSparseFeatures (feat_vec, x_space);
		double *prob_est = new double[2];
		predict_probability(train_model, x_space, prob_est);

		double prob_est2;
		prob_est2= predict(train_model, x_space);
		if (prob_est2==1){
			sum_1s++;
		}

		if(test_labels[k]==0){
			true_label.push_back(0);
		}
		else {
			true_label.push_back(test_labels[k]);
		}

		if(test_labels[k] == prob_est2){
			acc_1++;
		}
		raw_ranks2.push_back(prob_est[1]);
		k++;
	}
	accuracy = acc_1/(double)pd.parsed_data.size();
}



void LogReg::logreg_train(ParsedData pd){
	init_params();
	linear_model *train_model;
	problem prob;
	check_parameter(&prob,&param);
	get_traindata(&pd, &prob);
	train_model=train(&prob,&param);

	const char *model_file_name = "saved_model.model";
	int saved= save_model(model_file_name, train_model);
}

void LogReg::get_traindata(ParsedData *pd, problem *prob){
	prob->l = pd->parsed_data.size();
	prob->n = pd->parsed_data.back().feats.size();
	prob->bias=0.0;

	//initialize the problem
	double * y_test = CrossfileFuncts::get_yvec(&pd->parsed_data,1);
	prob->y=y_test;
	get_featurevec(pd,prob);
}

void LogReg::get_featurevec(ParsedData *pd, problem *prob){
	feature_node** x = new feature_node *[prob->l];

	int i =0;
	for(std::vector<ParsedLine>::iterator it= pd->parsed_data.begin(); it!=pd->parsed_data.end(); it++, i++){
		ParsedLine sample = *it;
		std::vector<double> feat_vec = sample.feats;
		int num_l=0;
		CrossfileFuncts::GetSparseFeatLength(feat_vec, num_l);
		feature_node *x_space = new feature_node[num_l+1];
		CrossfileFuncts::MakeSparseFeatures (feat_vec, x_space);
		x[i] = x_space;
	}
	prob->x = x;
}
