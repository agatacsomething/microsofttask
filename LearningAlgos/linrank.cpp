#include "linrank.h"
#include <iostream>
#include "crossfilefuncts.h"
#include <tuple>


LinRank:: LinRank(){
	pairwise_acc=0;
	ndcg_acc=0;
}

void LinRank::init_params(){
	param.solver_type=L2R_L2LOSS_RANKSVM;
	param.eps= 0.001;
	param.C=0.01;
	param.nr_weight=2;
	int weight_label[2];
	weight_label[0]=-1;
	weight_label[1]=1;

	double w_val=1;
	param.weight_label=weight_label;
	double weight[2];
	weight[0]=0.01;
	weight[1]=1;
	param.weight=weight;
	param.p=0.00001;
}

void LinRank::linrank_test(ParsedData pd, linear_model *train_model){
	double *dvec_t= new double[pd.parsed_data.size()];
	double *ivec_t = CrossfileFuncts::get_yvec(&pd.parsed_data,2);

	//Change data struct to Libsvm format
	std::vector<int> pred_labels;

	int k =0;
	int ones_sum=0;
	for(std::vector<ParsedLine>::iterator it= pd.parsed_data.begin(); it!=pd.parsed_data.end(); it++){
		ParsedLine sample = *it;
		std::vector<double> feat_vec = sample.feats;
		int num_l=0;
		CrossfileFuncts::GetSparseFeatLength(feat_vec, num_l);
		feature_node *x_space = new feature_node[num_l+1];
		CrossfileFuncts::MakeSparseFeatures (feat_vec, x_space);
		double prob_est, prob_est3;
		double *prob_est2 = new double[2];
		prob_est3=predict_probability(train_model, x_space, prob_est2);
		prob_est= predict(train_model, x_space);

		int guess;
		if(prob_est2[0]>=prob_est2[1]){
			guess=-1;
		}
		else{
			guess=1;
			ones_sum++;
		}

		dvec_t[k]=prob_est;
		raw_ranks2.push_back(prob_est);
		pred_labels.push_back(guess);
		k++;
	}

	double *result = new double[2];
	int *qvec= CrossfileFuncts::get_queryvec(&pd);
	int n =pd.parsed_data.size();
	eval_list(ivec_t,dvec_t,qvec,n,result);

	pairwise_acc = result[0]*100.0;
	ndcg_acc = result[1];
}



void LinRank::linrank_train(ParsedData pd){
	init_params();
	linear_model *train_model;
	problem prob;
	check_parameter(&prob,&param);
	get_traindata(&pd, &prob);
	train_model=train(&prob,&param);

	const char *model_file_name = "saved_lrrank_model.model";
	int saved= save_model(model_file_name, train_model);
}

void LinRank::get_traindata(ParsedData *pd, problem *prob){
	prob->l = pd->parsed_data.size();
	prob->n = pd->parsed_data.back().feats.size();
	prob->bias=1.0;

	//Change data struct to Libsvm format
	double * y_test = CrossfileFuncts::get_yvec(&pd->parsed_data,2);
	prob->y =y_test;
	get_featurevec(pd, prob);
	int * query = CrossfileFuncts::get_queryvec(pd);
	prob->query = query;
}

void LinRank::get_featurevec(ParsedData *pd, problem *prob){
	feature_node** x = new feature_node *[prob->l];

	int i =0;
	for(std::vector<ParsedLine>::iterator it= pd->parsed_data.begin(); it!=pd->parsed_data.end(); it++, i++){
		ParsedLine sample = *it;
		std::vector<double> feat_vec = sample.feats;

		int num_l=0;
		CrossfileFuncts::GetSparseFeatLength(feat_vec, num_l);
		feature_node *x_space = new feature_node[num_l+1];
		CrossfileFuncts::MakeSparseFeatures (feat_vec, x_space);
		x[i] = x_space;
	}
	prob->x = x;
}
