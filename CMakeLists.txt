#rm CMakeCache.txt; cmake -G"Eclipse CDT4 - Unix Makefiles" -DCMAKE_ECLIPSE_VERSION=4.4 -DLIBLINEAR_DIR=/usr/local/Cellar/liblinear/1.9.4
#rm CMakeCache.txt; cmake -G"Eclipse CDT4 - Unix Makefiles" -DCMAKE_ECLIPSE_VERSION=4.4 -DLIBLINEAR_SRC_DIR=../Libraries/liblinear-ranksvm-1.95 -DLIBSVMRANK_SRC_DIR=../Libraries/libsvm-ranksvm-3.20 ../

cmake_minimum_required(VERSION 2.8)
project(MicrosoftTask)

set(CMAKE_CXX_FLAGS "-std=c++11 -stdlib=libc++")

set(CMAKE_AUTOMOC ON)
find_package(Qt4 REQUIRED)

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)

#Adding SVM RANK
SET(LIBLINEAR_SRC_DIR "Libraries/liblinear-ranksvm-1.95")
SET(LIBSVMRANK_SRC_DIR "Libraries/libsvm-ranksvm-3.20")

INCLUDE_DIRECTORIES(UserInterface)
INCLUDE_DIRECTORIES(LearningAlgos)
INCLUDE_DIRECTORIES(Evaluation)
INCLUDE_DIRECTORIES(${LIBLINEAR_SRC_DIR})
INCLUDE_DIRECTORIES(${LIBSVMRANK_SRC_DIR})

ADD_LIBRARY(
    liblinear_svmrank_lib STATIC
    ${LIBLINEAR_SRC_DIR}/linear.o
    ${LIBLINEAR_SRC_DIR}/tron.o
    ${LIBLINEAR_SRC_DIR}/ranksvm.o
    ${LIBLINEAR_SRC_DIR}/blas/daxpy.o
    ${LIBLINEAR_SRC_DIR}/blas/ddot.o
    ${LIBLINEAR_SRC_DIR}/blas/dnrm2.o
    ${LIBLINEAR_SRC_DIR}/blas/dscal.o
)

ADD_LIBRARY(
    libsvmrank_lib STATIC
    ${LIBSVMRANK_SRC_DIR}/selectiontree.o
    ${LIBSVMRANK_SRC_DIR}/svm.o
    ${LIBSVMRANK_SRC_DIR}/svm-tron.o
    ${LIBSVMRANK_SRC_DIR}/blas/daxpy.o
    ${LIBSVMRANK_SRC_DIR}/blas/ddot.o
    ${LIBSVMRANK_SRC_DIR}/blas/dgemm.o
    ${LIBSVMRANK_SRC_DIR}/blas/dgemv.o
    ${LIBSVMRANK_SRC_DIR}/blas/dger.o
    ${LIBSVMRANK_SRC_DIR}/blas/dnrm2.o
    ${LIBSVMRANK_SRC_DIR}/blas/dscal.o
    ${LIBSVMRANK_SRC_DIR}/blas/dspmv.o
    ${LIBSVMRANK_SRC_DIR}/blas/lsame.o 
    ${LIBSVMRANK_SRC_DIR}/blas/xerbla.o  
)


set( HEADERS 
    UserInterface/MainWindow.h  
    LearningAlgos/preprocessdata.h
    UserInterface/qcustomplot.h
    LearningAlgos/logreg.h
    LearningAlgos/linrank.h
    LearningAlgos/crossfilefuncts.h
    LearningAlgos/libsvmrank.h
    Evaluation/evalresults.h
    Evaluation/compmethods.h

)

set( SOURCES 
    main.cpp
    UserInterface/MainWindow.cpp  
    UserInterface/qcustomplot.cpp
    LearningAlgos/preprocessdata.cpp
    LearningAlgos/logreg.cpp
    LearningAlgos/linrank.cpp
    LearningAlgos/crossfilefuncts.cpp
    LearningAlgos/libsvmrank.cpp
    Evaluation/evalresults.cpp
	Evaluation/compmethods.cpp
)




find_package(Boost COMPONENTS system filesystem chrono REQUIRED)
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR} ${Boost_INCLUDE_DIR})
QT4_WRAP_CPP(${PROJECT_NAME}_HEADERS_MOC ${${PROJECT_NAME}_HEADERS})
INCLUDE(${QT_USE_FILE})
ADD_DEFINITIONS(${QT_DEFINITIONS})


add_executable(${PROJECT_NAME} ${SOURCES}  ${HEADERS} ${${PROJECT_NAME}_HEADERS_MOC})
target_link_libraries(${PROJECT_NAME} ${QT_LIBRARIES})
target_link_libraries(${PROJECT_NAME} liblinear_svmrank_lib)
target_link_libraries(${PROJECT_NAME} libsvmrank_lib)
target_link_libraries(${PROJECT_NAME} ${Boost_LIBRARIES}  ${Boost_FILESYSTEM_LIBRARY} ${Boost_SYSTEM_LIBRARY} )