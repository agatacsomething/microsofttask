Video demonstration of running widget: https://youtu.be/5bpMJbuVN7Q



This project requires the following libraries to run: 
liblinear
libsvm
qt4
boost

warnings: 
1. Within the project folder liblinear and libsvm are included. 
These libraries are slightly modified from the original versions. 
2. The cmake folder assumes liblinear and libsvm are in the src directory

Specifications the original project was tested on:
os: apple osx 10.90
compiled using c++11, and gcc
environment: eclipse luna (4.4)

To make (using Cmake GUI): 
1. In terminal go to src/liblinear and "make"
2. In terminal go to src/libsvm and "make"
3. In cmake configure for "Eclipse Unix - Unix Makefiles"
4. Generate makefile. 

To make in terminal: 
1. In terminal go to src/liblinear and "make"
2. In terminal go to src/libsvm and "make"
3. Go to main folder (one folder above src)
4. mkdir build
5. cd build
6. cmake -G"Eclipse CDT4 - Unix Makefiles" ../src


