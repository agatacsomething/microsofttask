#include "MainWindow.h"

#include <QtGui/QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow mainWindow;
    mainWindow.resize(850, 650);
    mainWindow.setWindowTitle("Query Training App");
    mainWindow.show();

    return app.exec();
}
