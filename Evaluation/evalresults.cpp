#include "evalresults.h"

#include <iostream>

Evalresults::Evalresults(double pwacc, double ndcgacc): out_res(NULL), rank_res(NULL){
	pairwise_acc=pwacc;
	ndcg_acc=ndcgacc;
	acc=999999;
}

Evalresults::Evalresults(double accur): out_res(NULL), rank_res(NULL){
	acc=accur;
	pairwise_acc=999999;
	ndcg_acc=999999;
}

void Evalresults::calc_map(std::vector<double> raw_ranks, ParsedData pd, int which_train){
	rank_results(raw_ranks, pd, which_train);
	out_res = new Evaledres;

	for(std::vector<Rankresults>::iterator it = rank_res->begin(); it!=rank_res->end(); it++){
		Rankresults querysample = *it;
		int k =1;
		int pos_count = 0;
		std::vector<double> query_maps;
		for(std::vector<double>::iterator it2 = querysample.true_relev.begin(); it2 != querysample.true_relev.end(); it2++,k++){
			if (*it2==1){
				pos_count++;
				query_maps.push_back(1.0/k);
			}
		}
		double total_map = std::accumulate(query_maps.begin(),query_maps.end(),0.0);
		if(pos_count<1){
			pos_count=1;
		}
		out_res->revel_count.push_back(pos_count);
		total_map= total_map/pos_count;
		out_res->map_vals_raw.push_back(total_map);
	}

	double total_map = std::accumulate(out_res->map_vals_raw.begin(),out_res->map_vals_raw.end(),0.0);
	total_map= total_map/rank_res->size();
	out_res->total_map = total_map;


}

void Evalresults::rank_results(std::vector<double> raw_ranks, ParsedData pd, int which_train){
	rank_res = new std::vector<Rankresults>;
	double * test_labels = CrossfileFuncts::get_yvec(&pd.parsed_data, which_train);

	int k = 0;

	for(std::vector<std::string>::iterator it2 = pd.data_stats.uniq_queries.begin(); it2!=pd.data_stats.uniq_queries.end(); it2++){
		std::string str1 = *it2;
		std::vector<std::tuple<double, double, std::string>> quer_subset;
		for(std::vector<ParsedLine>::iterator it = pd.parsed_data.begin(); it!=pd.parsed_data.end(); it++){
			ParsedLine sample = *it;
			if (str1.compare(sample.qid) == 0){
				quer_subset.push_back(std::make_tuple(raw_ranks[k],test_labels[k],sample.docid));
				k++;
			}
		}
		rank_results_query(quer_subset);
	}
}

void Evalresults::rank_results_query(std::vector<std::tuple<double, double, std::string>> quer_subset){
	sort(quer_subset.begin(),quer_subset.end(),
	       [](const std::tuple<double,double,std::string>& a,
	       const std::tuple<double,double,std::string>& b) -> bool
	       {
	         return std::get<0>(a) > std::get<0>(b);
	       });

	Rankresults sample;
	for(std::vector<std::tuple<double, double, std::string>>::iterator it = quer_subset.begin(); it != quer_subset.end(); it++){
		std::tuple<double, double, std::string> sr = *it;
		sample.raw_rank.push_back(std::get<0>(sr));
		sample.true_relev.push_back(std::get<1>(sr));
		sample.unique_qid.push_back(std::get<2>(sr));
	}
	rank_res->push_back(sample);


}
