/**
 * Class: Evalresults
 * Description: Class for evaluating ranked results from tested data
 * Functions:
 * 		calc_map;
 * 		Description: Function for calculating mean average precision for each query
 * 		Sample Usage:
 * 			Evalresults *eval_test_results = new Evalresults;
 * 			ParsedData pd; 					//preloaded data
 * 			int which_train =1;				//used to correctly get the label vector
 * 			std::vector<double> raw_ranks   //vector of the raw rank values for each query
 *
 * 			calc_map(raw_ranks, pd, which_train);
 * 		Output:
 * 			void, but MAP results written to out_res struct
 * Output:
 * 		Evaledres *out_res; 				//struct of mean average precision results
 * 		std::vector<Rankresults> *rank_res;	//a vector of struct Rankresults which contains the ranked results for a single unique query set
 * 		double pairwise_acc; 				//pairwise accuracy copied from testing
 * 		double ndcg_acc; 					//ndcg copied from testing
 * 		double acc; 						//accuracy copied from testing
 */

#ifndef EVALRESULTS_H
#define EVALRESULTS_H

#include <vector>
#include <tuple>
#include <numeric>
#include <algorithm>

#include "preprocessdata.h"
#include "crossfilefuncts.h"

struct Rankresults{
	std::vector<std::string> unique_qid;
	std::vector<double> raw_rank;
	std::vector<double> true_relev;
};

struct Evaledres{
	double total_map;
	std::vector<double> map_vals_raw;
	std::vector<int> revel_count;
};

class Evalresults{
public:
	Evalresults(double accur);
	Evalresults(double pwacc, double ndcgacc);

	Evaledres *out_res;
	std::vector<Rankresults> *rank_res;

	double pairwise_acc;
	double ndcg_acc;
	double acc;

	void calc_map (std::vector<double> raw_ranks, ParsedData pd, int which_train);

private:
	void rank_results_query(std::vector<std::tuple<double, double, std::string>> quer_subset);
	void rank_results(std::vector<double> raw_ranks, ParsedData pd, int which_train);
};

#endif
