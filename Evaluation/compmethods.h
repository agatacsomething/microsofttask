/**
 * Class: COMPMETHODS
 * Description: Comparison of algorithms using the sign test and z-test
 * Input: Parsed training or testing data.
 * 		ParsedData *pd,
 * Output:
 * 		std::vector<double> sign_test_vec; 		// a vector of the sign test differences
 * 		double final_prob 						// sign test probablity that method 1 > method 2
 * 		double ztest_val;						// a vector of the sign test differences
 * Functions:
 * 		Two main functions, one to perform sign test and the other to perform the z-test
 *
 * 		sign_test
 * 		Description: Performs the aggressive sign test, to see the probability the two sets come from a different distribution
 * 		Sample Usage:
 * 			Evaledres *evel_res1;				//pretrained and preevaluated results of algo 1
 * 			Evaledres *evel_res2;				//pretrained and preevaluated results of algo 2
 * 			sign_test(evel_res1, evel_res2);
 *
 * 		ztest
 * 		Description: Performs the less aggressive ztest, to see the probability the two sets come from a different distribution
 * 		Sample Usage:
 * 			Evaledres *evel_res1;				//pretrained and preevaluated results of algo 1
 * 			Evaledres *evel_res2;				//pretrained and preevaluated results of algo 2
 * 			ztest(evel_res1, evel_res2);
 */


#ifndef COMPMETHODS_H
#define COMPMETHODS_H

#include "evalresults.h"

class Compmethods{

public:
	std::vector<double> sign_test_vec;
	double final_prob, ztest_val;

	Compmethods();
	void sign_test(Evaledres *evel_res1, Evaledres *evel_res2);
	void ztest(Evaledres *evel_res1, Evaledres *evel_res2);
private:
	void get_ztvec(Evaledres *evel_res1, Evaledres *evel_res2);
};

#endif
