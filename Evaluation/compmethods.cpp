#include "compmethods.h"
#include <iostream>
#include <math.h>
#include <cmath>
#include <random>

Compmethods::Compmethods(){
	final_prob = 999999;
	ztest_val= 999999;
}

double factorial(double innum){
	double outnum=1;
	for(int k = 1; k<=innum; k++){
		outnum = outnum*k;
	}
	return outnum;
}

void Compmethods::sign_test(Evaledres *evel_res1, Evaledres *evel_res2){
	get_ztvec(evel_res1, evel_res2);

	std::vector<double>::iterator it;
	int i =0;
	for (it = sign_test_vec.begin(); it!=sign_test_vec.end(); it++, i++){
	}

	double k_fac=factorial(sign_test_vec.size());

	int start_val;
	for(std::vector<double>::iterator it= sign_test_vec.begin(); it!= sign_test_vec.end(); it++){
		if(*it ==1){
			start_val++;
		}
	}

	std::vector<double> pr;
	for(int k = start_val; k<=sign_test_vec.size(); k++){
		double val = k_fac/(factorial(k)*factorial(sign_test_vec.size()-k))*pow(0.5, sign_test_vec.size());
		pr.push_back(val);
	}

	final_prob = std::accumulate(pr.begin(), pr.end(), 0.0);
}

void Compmethods::get_ztvec(Evaledres *evel_res1, Evaledres *evel_res2){
	std::vector<double>::iterator it;
	int i =0;
	for (it = evel_res1->map_vals_raw.begin(); it!=evel_res1->map_vals_raw.end(); it++, i++){
		double diff =*it-evel_res2->map_vals_raw[i];
		if (diff>0){
			sign_test_vec.push_back(1.0);
		}
		else if(diff==0){
			sign_test_vec.push_back(0.0);
		}
		else{
			sign_test_vec.push_back(-1.0);
		}
	}

}

double CalculateMean(std::vector<double> value)
{
    double sum = 0;
    for(int i = 0; i < value.size(); i++)
        sum += value[i];
    return (sum / value.size());
}

double CalculateVariance(std::vector<double> value)
{
    double mean = CalculateMean(value);

    double temp = 0;
    for(int i = 0; i < value.size(); i++)
    {
         temp += (value[i] - mean) * (value[i] - mean) ;
    }
    return temp / value.size();
}

double GetStandardDeviation(std::vector<double> value)
{
	return sqrt(CalculateVariance(value));
}

double GetStandardDeviationCombo(double std1, double std2, int tot1, int tot2)
{
	double top =(tot1-1)*std1*std1 + (tot2-1)*std2*std2;
	double bottom = tot1 + tot2 - 2;
	return sqrt(top/bottom);
}

void Compmethods::ztest(Evaledres *evel_res1, Evaledres *evel_res2){
	double mean1= CalculateMean(evel_res1->map_vals_raw);
	double mean2= CalculateMean(evel_res2->map_vals_raw);
	double std1 = GetStandardDeviation(evel_res1->map_vals_raw);
	double std2 = GetStandardDeviation(evel_res2->map_vals_raw);
	double std12 =GetStandardDeviationCombo(std1, std2, evel_res1->map_vals_raw.size(), evel_res2->map_vals_raw.size());
	double num = std::abs(mean1-mean2);
	double bot = std12*sqrt(1.0/evel_res1->map_vals_raw.size() + 1.0/evel_res2->map_vals_raw.size());
	ztest_val = num/bot;

}
